

import pandas as pd
import numpy as np

def compareGT1(diff ,th):
    '''
    function is following the rules given by client for finding trend
    ''' 
    if diff > -th and diff < th:
        return 0
    elif diff >= th:  
        return 1
    elif diff <= -th :  
        return -1

def TH_GT1(df,curr_month,prev_month,
              x1,x2,x3,x4,rof=1):
    
    th1 = 1 * x1
    th2 = 0.5 * x2
    th3 = 0.2 * x3
    th4 =0.1 * x4
    prev_val = df[prev_month]
    
    if prev_val< 0:
        return -1
    if prev_val == 0 and df[curr_month]>0:
        return 1
    if prev_val == 0 and df[curr_month] < 0:
        return -1
    if prev_val == 0 and df[curr_month]==0:
        return 0
    
    if prev_val >= 10: #70
        diff = (np.round(df[curr_month])-np.round(df[prev_month])) 
        return compareGT1(diff,th1)
    
    elif prev_val >= 5 and prev_val < 10:
        diff =  np.round(df[curr_month],rof)- np.round(df[prev_month],rof)
        diff = np.round(diff, rof)
        return compareGT1(diff,th2)
    
    elif prev_val >= 1 and prev_val < 5:
        diff = np.round(df[curr_month],rof)- np.round(df[prev_month],rof) 
        diff = np.round(diff, rof )
        return compareGT1(diff,th3)
        
    else:
        diff = np.round( df[curr_month], rof )- np.round(df[prev_month], rof)
        diff = np.round(diff , 1)
        return compareGT1(diff,th4)
    

def Trend_df(df ,col1 ,col2):
    if df[col1] == df[col2]:
        if df[col1] ==1:
            return 1
        elif df[col2] == -1:
            return -1
        else:
            return 0
    else:
        return 0    
    
def Classification( df ,x1,x2,x3,x4,month, prev_month, pvv_month ,
                     cv=True):
    
    df['TH_prev'] = df.apply(TH_GT1,args=[ prev_month,pvv_month,
                                        x1,x2,x3,x4],axis=1)
    df['TH'] = df.apply( TH_GT1 ,args=[month,prev_month,
                                    x1,x2,x3,x4],axis=1)
    df[ month+"_GT1"] = df.apply(Trend_df,args=['TH','TH_prev'],axis=1)
    return df



def compare_GT2(df ,col1 ,col2,col3,th,class_,rof):
    if rof==0:
        clf = (np.round(df[col3])-np.round(df[col1]))
    else:
        clf = (np.round(df[col3],rof)-np.round(df[col1],rof))

    if clf >= th  and df[col2]-df[col1]>0  and df[col3]-df[col2]>0:  
        return 1
     
    elif clf <= -th and df[col2]-df[col1]<0  and df[col3]-df[col2]<0:  
        return -1
    else:
        return df[class_]

def GT2(df,x1,x2,x3,x4,class_,col,mid_col,prev):
     
    th1 = 1*x1 
    th2 = 0.5 *x2 
    th3 = 0.2 *x3
    th4 =0.1 *x4
    prev_val = df[prev]
    
    if df[class_]==0:
        if prev_val< 0:
            return -1
        if prev_val == 0 and df[col]>0 and df[mid_col]-df[prev]>0:
            return 1
        if prev_val == 0 and df[col] < 0  and df[mid_col]-df[prev]<0:
            return -1
        if prev_val == 0 and df[col]==0 and df[mid_col]-df[prev]==0:
            return df[class_]

        if prev_val >= 10: #70
             return compare_GT2(df,prev,mid_col,col,th1,class_,0)
        
        elif prev_val >= 5 and prev_val < 10:
            return compare_GT2(df,prev,mid_col,col,th2,class_,1)
       
        elif prev_val >= 1 and prev_val < 5:
            return compare_GT2(df,prev,mid_col,col,th3,class_,1)

        else:
            return compare_GT2(df,prev,mid_col,col,th4,class_,1)
    else:
        return df[class_]


    
def ClassificationGT2( df ,x1,x2,x3,x4,month  ,prev_month, pvv_month ,
                    cv=True):
    df[month+'_GT2'] = df.apply(GT2 ,args=[x1,x2,x3,x4,month+'_GT1',month , prev_month,pvv_month],
                            axis=1 )
    return df





# if name=='__main__':
# 	TrainingDataPath="../Training_data(sep).csv"
# 	df = pd.read_csv(TrainingDataPath)
# 	df['y_curr']= df['Combined']
# 	df['y_next'] = df.groupby(by=['Brand','Center'])['y_curr'].shift(-1)

# 	df['y_target']=df.groupby(by=['Brand','Center'])['y_curr'].shift(-2)
# 	df=df[['Brand','Center','ds','B1','B2','B3',
# 	       'Smoker Share','y_curr','y_next','y_target']]


# 	df =  Classification(df,1,1,1,1,'y_target','y_next','y_curr')
# 	df = ClassificationGT2(df ,2,2,2,2,'y_target','y_next','y_curr') 

# 	 