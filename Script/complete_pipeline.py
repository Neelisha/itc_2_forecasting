 

import pandas as pd
from config import config
import numpy as np
import os
 
import torch
import torch.nn as nn
from fastai import *
import fastai

torch.manual_seed(0)
 
import Generate_LSTM_Result
from Generate_CLF_Result import getResultClf
import Classification_label
from Training_data_prep_regressor import Prep_forecast_training_data
from inception import *
 
# once file is Prediction.csv is prepared no need to run this function
# final_df =Prep_forecast_training_data()

 
#  Training data prep for classification
# df = pd.read_csv(config['TrainingCsvPath'])
# df['y_curr']= df['Combined']
# df['y_next'] = df.groupby(by=['Brand','Center'])['y_curr'].shift(-config['model_lstm_param']['jlasty'])
# df['y_target']=df.groupby(by=['Brand','Center'])['y_curr'].shift(-config['model_lstm_param']['jtargety'])

# df=df[['Brand','Center','ds','B1','B2','B3','Smoker Share','y_curr','y_next','y_target']]
# df = Classification_label.Classification(df,1,1,1,1,'y_target','y_next','y_curr')
# df = Classification_label.ClassificationGT2(df ,2,2,2,2,'y_target','y_next','y_curr') 
# enddate = config['model_lstm_param']['enddate']
# df.drop(['TH_prev','TH','y_target_GT1'],axis=1,inplace=True)
# df.rename(columns={'y_target_GT2':'Target'},inplace=True)
# df.to_csv( config['TrainingClassifyPath'],index=False)
# print("Supervised data for classification is prepared for t+2 and could be modified further for t+3, csv saved to {}".format( config['TrainingClassifyPath']))

print(config['model_lstm_param'],config['model_clf_param'])

Result =  Generate_LSTM_Result.Predict_Save(**config['model_lstm_param'] )
  
lstm_result= Result.getResultLSTM()

# check predicting t+2 or t+3

#need to change the prediction sheeet path from lstm
 
lstm_result = pd.read_csv(config['model_lstm_param']['base_path']+"Prediction.csv")
lstm_pred = lstm_result[['Brand','Center','y_pred']]
classification_df = pd.read_csv(config['TrainingClassifyPath'])

if  config['model_lstm_param']['jtargety']==3:
    classification_df['y_target'] = classification_df.groupby(by=['Brand','Center'])['y_target'].shift(-1)
    classification_df['Target'] = classification_df.groupby(by=['Brand','Center'])['Target'].shift(-1)


_filteredDF = classification_df[classification_df['ds']==config['model_lstm_param']['enddate']].reset_index(
                                            drop=True)
_filteredDF =_filteredDF.merge(lstm_pred,on=['Brand','Center'])

classification_df = classification_df.merge(_filteredDF[['Brand', 'Center', 'ds','y_pred']],
                                        on=['Brand','Center','ds'],
      how='outer')


classification_df['y_target'] = np.where(classification_df['ds']==config['model_lstm_param']['enddate'], 
                                     np.nan, classification_df['y_target'])
classification_df['y_target'].fillna(classification_df['y_pred'],inplace=True)
classification_df.drop('y_pred',axis=1,inplace=True)
classification_df= classification_df.dropna().reset_index(drop=True) 
classification_df.drop_duplicates(keep='last',inplace=True) 
classification_df=classification_df.reset_index(drop=True)

mask = (classification_df['ds'] <=config['model_lstm_param']['enddate'])
classification_df = classification_df[mask].reset_index(drop=True)

classification_df=classification_df[['Brand','Center','ds','B1','B2','B3','Smoker Share',
   'y_curr','y_next','y_target','Target']]

for c in ['B1','B2','B3','Smoker Share','y_curr','y_next','y_target' ]:
    classification_df[c] =classification_df[c].astype(float)

 
# %%time
clf_result = getResultClf(classification_df)
 
### combining the sheet of prediction value and class

clf = pd.read_csv(config['clf_model']+"/Classification.csv")[['Brand','Center','Pred_class']]
 
lstm = pd.read_csv(config['model_lstm_param']['base_path']+"/Prediction.csv")[['Brand','Center','y_pred']]
 
month = "Jan'20"

result = lstm.merge(clf,on=['Brand','Center'])

result.rename(columns={'y_pred':month+"(P_value)",
                      'Pred_class':month+"(P_class)"},inplace=True)
 

combined = pd.read_excel(config['NewItcExcel'])
 

# final = pd.merge([combined,result],left_on=['City','Brand-Codes'],right_on=['Brand','Center'] )

final = combined.merge(result,right_on=['Brand','Center'],left_on=['Brand-Codes','City'],how='left').drop(
    ['Brand','Center'],axis=1)

final.to_csv("../"+month+".csv",index=False)
 