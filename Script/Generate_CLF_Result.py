import pandas as pd
import numpy as np
import os

from config import config
from inception import *
import torch
import torch.nn as nn
import fastai
from fastai import *
torch.manual_seed(0)
from progressbar import  ProgressBar

ntest = 1   # number of data points to test
epochs = 10
depth = 3
dev = torch.device("cuda") if torch.cuda.is_available() else torch.device("cpu")


def mkdir_(file):
	if not os.path.exists(file):
		os.mkdir(file)

def train_global(df):
	print("training global classifier for epoch {}".format(epochs))
	num_class = config['model_clf_param']['num_classes']
	brand_center =  df.groupby(['Brand','Center']).size().reset_index().values
	num_feats = len( df.columns)-4 #remove 4 col :(ds,Brand,center, 1 more)

	model = create_inception(1,num_class)
	gpu =torch.cuda.is_available()

	if gpu:
		model.double().cuda()
		class_wt = torch.cuda.DoubleTensor(config['model_clf_param']['wt_cls']) 
	else:
		class_wt = torch.DoubleTensor(config['model_clf_param']['wt_cls']) 
		model.double()

	criterion = nn.CrossEntropyLoss(weight=class_wt)
	optimizer = optim.Adam(model.parameters(), lr=5e-3)

	mkdir_(config['clf_model'])
	mkdir_(config['clf_model']+"/global")
	pbar =ProgressBar()
	for ep in pbar(range(epochs)):
		g_loss = 0
		count  = 0
		for b, c, n in brand_center:
			_df = df[(df['Brand'] == b) & (df['Center'] == c)].reset_index(drop=True)
			ts = n
			if ts > 10:
				X_train = _df.iloc[:ts-ntest,3:-1].values
				y_train = _df.iloc[:ts-ntest,-1].values
				y_train = y_train+1
				y_train = y_train.astype(int)
				outputs = model(torch.from_numpy(X_train).view(ts-ntest,1,num_feats).to(dev)) 
				loss = criterion(outputs, torch.from_numpy(y_train).to(dev))
				optimizer.zero_grad()
				loss.backward()
				optimizer.step()
				g_loss += loss.data.cpu().numpy()
				count += 1

		print ("Epoch {} : {}".format(ep, g_loss/count))
		checkpoint = {'model': model.state_dict(),
			'optimizer': optimizer.state_dict()}

		torch.save(checkpoint, os.path.join(config['clf_model']+"/global", "model_%d.pth"%(ep+1)))


def train_local(df):
	'''since the when training locally the model was not improving'''
	print("training local")
	brand_center =  df.groupby(['Brand','Center']).size().reset_index().values
	num_feats = len( df.columns)-4 #("Brand,,Center,target,ds not considere as important feature")
	num_class = config['model_clf_param']['num_classes']
	mkdir_(config['clf_model'])
	mkdir_(config['clf_model']+"/local")
	print("training local model")
	pbar = ProgressBar()
	for b, c, n in pbar(brand_center):
		_df = df[(df['Brand'] == b) & (df['Center'] == c)].reset_index(drop=True)
		ts = n
		if ts > 10:
			model = create_inception(1,num_class)
			gpu =torch.cuda.is_available()
			if gpu:
				model.double().cuda()
				class_wt = torch.DoubleTensor(config['model_clf_param']['wt_cls']).cuda()
			else:
				class_wt = torch.DoubleTensor(config['model_clf_param']['wt_cls']) 
				model.double()
			
			criterion = nn.CrossEntropyLoss(weight=class_wt)
			optimizer = optim.Adam(model.parameters(), lr=5e-3)
			checkpoint = torch.load(os.path.join(config['clf_model']+"/global/","model_{}.pth".format(epochs)))
			model.load_state_dict(checkpoint['model'])

			X_train = _df.iloc[:ts-ntest,3:-1].values
			y_train = _df.iloc[:ts-ntest,-1].values
			y_train = y_train+1
			y_train = y_train.astype(int)

			outputs = model(torch.from_numpy(X_train).view(ts-ntest,1,num_feats).to(dev))
			loss = criterion(outputs, torch.from_numpy(y_train).to(dev))

			optimizer.zero_grad()
			loss.backward()
			optimizer.step()
				
			checkpoint = {'model': model.state_dict(),
				'optimizer': optimizer.state_dict()}
			torch.save(checkpoint, os.path.join(config['clf_model']+"/local", "model_{}_{}.pth".format(b, c)))


def predict(df):
	brand_center =  df.groupby(['Brand','Center']).size().reset_index().values
	num_feats = len( df.columns)-4 #("Brand,,Center,target,ds not considere as important feature")
	num_class = config['model_clf_param']['num_classes']
	result_df = pd.DataFrame()
	actual = []
	pred = []
	brand = []
	center = []
	num_class = config['model_clf_param']['num_classes']
	for b, c, n in brand_center:
		_df = df[(df['Brand'] == b) & (df['Center'] == c)].reset_index(drop=True)
		ts = n
		if ts > 10:
			X_test = _df.iloc[:ts,3:-1].values
			y_test = _df.iloc[:ts,-1].values
			y_test = y_test+1
			y_test = y_test.astype(int)
			temp = y_test
			y_test = np.zeros((ts, num_class))
			y_test[np.arange(ts), temp] = 1
			model = create_inception(1,num_class  ) 
			model.double()			 
			checkpoint = torch.load((config['clf_model']+"/local/model_{}_{}.pth").format(b, c))
			model.load_state_dict(checkpoint['model'])
			model.eval()
			outputs = model(torch.from_numpy(X_test).view(ts,1,num_feats) )
			outputs = outputs.data.numpy()
			outputs = outputs.argmax(axis=1)
			y_test = y_test.argmax(axis=1)
			outputs = outputs[-ntest:]
			y_test = y_test[-ntest:]
			actual.extend(y_test)
			pred.extend(outputs)
			brand.append(b)
			center.append(c)

	result_df = pd.DataFrame()

	result_df['Brand'] =brand

	result_df['Center'] =center
 
	result_df["Pred_class"] = np.array(pred)-1
	result_df['Actual'] = np.array(actual)-1
	print("classification results saved to : {}".format(config['clf_model']+"/Classification.csv"))
	result_df.to_csv(config['clf_model']+"/Classification.csv",index=False)

	return result_df


def getResultClf(df):

	if config['model_clf_param']['train_global']:
		train_global(df.copy())
	
	train_local(df.copy())
	df_result = predict(df.copy())
	return df_result

# if __name__ == '__main__':
	
# 	lstm_result = pd.read_csv(config['model_lstm_param']['base_path']+"Prediction.csv")
# 	lstm_pred = lstm_result[['Brand','Center','y_pred']]
# 	classification_df = pd.read_csv(config['TrainingClassifyPath'])

# 	if  config['model_lstm_param']['jtargety']==3:
# 		classification_df['y_target'] = classification_df.groupby(by=['Brand','Center'])['y_target'].shift(-1)
# 		classification_df['Target'] = classification_df.groupby(by=['Brand','Center'])['Target'].shift(-1)


# 	_filteredDF = classification_df[classification_df['ds']==config['model_lstm_param']['enddate']].reset_index(
# 												drop=True)
# 	_filteredDF =_filteredDF.merge(lstm_pred,on=['Brand','Center'])

# 	classification_df = classification_df.merge(_filteredDF[['Brand', 'Center', 'ds','y_pred']],
# 											on=['Brand','Center','ds'],
# 		  how='outer')


# 	classification_df['y_target'] = np.where(classification_df['ds']==config['model_lstm_param']['enddate'], 
# 										 np.nan, classification_df['y_target'])
# 	classification_df['y_target'].fillna(classification_df['y_pred'],inplace=True)
# 	classification_df.drop('y_pred',axis=1,inplace=True)

# 	classification_df.dropna(inplace=True) 

# 	classification_df.drop_duplicates(keep='last',inplace=True) 

# 	classification_df=classification_df.reset_index(drop=True)

# 	mask = (classification_df['ds'] <=config['model_lstm_param']['enddate'])
# 	classification_df = classification_df[mask].reset_index(drop=True)

# 	classification_df=classification_df[['Brand','Center','ds','B1','B2','B3','Smoker Share',
# 	   'y_curr','y_next','y_target','Target']]

# 	for c in ['B1','B2','B3','Smoker Share','y_curr','y_next','y_target' ]:
# 		classification_df[c] =classification_df[c].astype(float)
	
# 	clf_result = getResultClf(classification_df)

# 	print(clf_result)