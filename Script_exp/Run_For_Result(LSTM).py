#!/usr/bin/env python
# coding: utf-8

# ###  parameter description

#  
# '''
# jlasty= 1 (int): shift combined sales value by -1,  to make a new feature 
#                 "last observed combined sales value" for the current date to
#                 predict future sales.
# 
# jtargety= 2 (int): n steps ahead we want to make prediction of sales value
#                    with the features of current date. 
# 
# enddate = "2019-04-01" (str): Date of which features to be used for predicting future
#                             eg-we will use features of "2019-04-01" ,last observed
#                             combined sale value of "2019-05-01"(jlasty=1) and will predict 
#                             combined sales value for "2019-06-01"(jtargety=2).
# 
# <b>trainglobal= True , When you want to train global model.</b>
# 
# trainingPath= Path of training csv file 
# 
# sartdate =(str) date from where the training should start
# 
# prev_time_steps=(int) number of time-steps from history you want to look for predicting future
# 
# averagewindow =(int) number of input sequence you want to feed in one-instance of LSTM.
# trainpercent =(float between 0 to 1) percentage split between training and validation data.
# 
# base_path = path of the folder where you want model and prediction file to be saved.
# 
# globalmodelpath=   Name of the folder for storing global model inside base_path
# 
# localmodelpath= Name of the folder for storing local model inside base_path.
# 
# csvpredictpath=  Name of the folder for storing predicted csv files inside base_path.
# 
# normalise_y =  True if you want to normalise y
# normalise_feat= True if you want to normalise smoker share feature
# 
# '''
# 
 
import Generate_Result


Result = Generate_Result.Predict_Save(jlasty=1,jtargety= 2,enddate="2019-06-01",
                                      trainingPath="../Training_data(sep).csv",
                                      trainglobal=True,
                                     base_path="../Aug2(season-1hot)/")

df= Result.generateResult()

