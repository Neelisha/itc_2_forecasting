
''' 
For new prediction update all the variable.
''' 
config ={
		#{  old_name:new_name }
		'RenameBrandAlert' : {"Brand-278977":"Brand-632181",
                       "Brand-30092":"Brand-88494",
                       "Brand-262799":"Brand-807608",
                       "Brand-923288":"Brand-530586",
                       	'Brand-119802': 'Brand-661798'
                      },

		'RenameBrandCombined' : {
	                          'Brand-769961':'Brand-379587',
	                        
	                         },
	    

	    #path to store the updated retail csv holding new months of retail data
		'UpdatedRetailPath' : "../Data/Retail_Nov19/",
		
		#Path to store the processed data
		'ProcessedPath' : "../Data/Processed_Data/",

		#folder having last used retail csv files
		'OldRetailFolder' : "../Data/Retail_Aug19/",

		#new retail excel path
		'NewRetailExcel' : "../Data/Sep'19-Nov'19 Retail Data - Anonymized.xlsx",

		#new ITC sales data excel sheet
		'NewItcExcel':"../Data/Sales Data Jan17-Dec19_Anonymised.xlsx",

		#new IMRB data (other features data like smoker share)
		'FeatureFilePath' : "../Data/Alert Data from Convergence - Anonymized (Jan'16 - Nov'19) - C1.xlsx",

		#sedtination path for storing supervised training data
		'TrainingCsvPath' :"../Training_data_regressor.csv",

		'TrainingClassifyPath':"../Training_data_classification.csv",


		#List of new month to update old retail csv with new Retail_values month ex(previous retail data was of till 819 and we get the new retail fil) 
		'MMYYlist': [919,1019,1119],


		#month'year name of the  last date in ITC combined data,
		'LastMonth':"Dec'19" , #used in feature_combine function,
		#update with new dates
		'GlobalDates':['2017-01-01','2017-02-01','2017-03-01',
		           	'2017-04-01','2017-05-01','2017-06-01','2017-07-01','2017-08-01',
		           	'2017-09-01','2017-10-01','2017-11-01','2017-12-01',
		           	'2018-01-01','2018-02-01','2018-03-01',
		           	'2018-04-01','2018-05-01','2018-06-01','2018-07-01','2018-08-01',
		           	'2018-09-01','2018-10-01','2018-11-01','2018-12-01',
		            '2019-01-01','2019-02-01','2019-03-01','2019-04-01','2019-05-01',
		            '2019-06-01','2019-07-01','2019-08-01','2019-09-01','2019-10-01',
		            '2019-11-01','2019-12-01'
		            ],


		#upadte with new dates
		#converting MMYY format to date stamp
		'DateStamp':{117:'2017-01-01',217:'2017-02-01',
		            317:'2017-03-01',417:'2017-04-01',
		            517:'2017-05-01',617:'2017-06-01',
		            717:'2017-07-01',817:'2017-08-01',
		            917:'2017-09-01',1017:'2017-10-01',
		            1117:'2017-11-01',1217:'2017-12-01',
		            118:'2018-01-01',218:'2018-02-01',
		            318:'2018-03-01',418:'2018-04-01',
		            518:'2018-05-01',618:'2018-06-01',
		            718:'2018-07-01',818:'2018-08-01',
		            918:'2018-09-01',1018:'2018-10-01',
		            1118:'2018-11-01',1218:'2018-12-01',
		           119:'2019-01-01',219:'2019-02-01',
		            319:'2019-03-01',419:'2019-04-01',519:'2019-05-01',
		            619:'2019-06-01',719:'2019-07-01',819:'2019-08-01',
		           919:'2019-09-01',
		           1019:'2019-10-01',
		           1119:'2019-11-01',
		           1219:'2019-12-01'},

		#Rename columns of DataFrame
		'MonthToDate' : {'City':'Center','Brand-Codes':'Brand Name',
		                   "Jan'17":'2017-01-01', "Feb'17":'2017-02-01',
		                   "Mar'17":"2017-03-01", "Apr'17":'2017-04-01', 
		                   "May'17":"2017-05-01","Jun'17":'2017-06-01',
		                   "Jul'17":"2017-07-01","Aug'17":'2017-08-01',
		                   "Sep'17":"2017-09-01","Oct'17":'2017-10-01',
		                   "Nov'17":'2017-11-01', "Dec'17":'2017-11-01',
		                   " Jan'18":'2018-01-01'," Feb'18":'2018-02-01'," Mar'18":'2018-03-01'
		                   , "  Apr'18":'2018-04-01', " May'18":'2018-05-01', " June '18":'2018-06-01',
		                   " July'18":'2018-07-01', " Aug'18":'2018-08-01', " Sep'18":'2018-09-01',
		                   " Oct'18":'2018-10-01', " Nov'18":'2018-11-01', " Dec'18":'2018-12-01',
		                  "Jan'19":"2019-01-01","Feb'19":"2019-02-01",
		                  "Mar'19":"2019-03-01" ,"Apr'19":"2019-04-01" ,"May'19":"2019-05-01",
		                "Jun'19":"2019-06-01" ,"Jul'19":"2019-07-01" ,"Aug'19":"2019-08-01",
		                "Sep'19":"2019-09-01","Oct'19":"2019-10-01","Nov'19":"2019-11-01","Dec'19":"2019-12-01"
		                },


	

'model_clf_param':{ 
					#number of classes
					'num_classes':3,
					#Classwise weightage given for loss calculation
					"wt_cls":[7,1,5],

					"train_global":True,
					}
}

config['model_lstm_param'] = {'jlasty' : 1 ,
					'jtargety': 2,
					'enddate':"2019-11-01",
					'trainglobal': True,
					'base_path':"../Jan_20/"
					}

#don't change
config['clf_model']=config['model_lstm_param']['base_path']+"Predicted_classifier"


config['model_lstm_param']['trainingPath'] = config['TrainingCsvPath']
# '''
	# jlasty= 1 (int): shift combined sales value by -1,  to make a new feature 
	#                 "last observed combined sales value" for the current date to
	#                 predict future sales.
	# 
	# jtargety= 2 (int): n steps ahead we want to make prediction of sales value
	#                    with the features of current date. 
	# 
	# enddate = "2019-04-01" (str): Date of which features to be used for predicting future
	#                             eg-we will use features of "2019-04-01" ,last observed
	#                             combined sale value of "2019-05-01"(jlasty=1) and will predict 
	#                             combined sales value for "2019-06-01"(jtargety=2).
	# 
	# <b>trainglobal= True , When you want to train global model.</b>
	# 
	# trainingPath= Path of training csv file 
	# 
	# sartdate =(str) date from where the training should start
	# 
	# prev_time_steps=(int) number of time-steps from history you want to look for predicting future
	# 
	# averagewindow =(int) number of input sequence you want to feed in one-instance of LSTM.
	# trainpercent =(float between 0 to 1) percentage split between training and validation data.
	# 
	# base_path = path of the folder where you want model and prediction file to be saved.
	# 
	# globalmodelpath=   Name of the folder for storing global model inside base_path
	# 
	# localmodelpath= Name of the folder for storing local model inside base_path.
	# 
	# csvpredictpath=  Name of the folder for storing predicted csv files inside base_path.
	# 
	# normalise_y =  True if you want to normalise y
	# normalise_feat= True if you want to normalise smoker share feature
	# 
	# COL = list of features to be used in prediction
	# '''
	# 