import csv,datetime,random, os, re
import pandas as pd 
import numpy as np
import torch , random
from torch.autograd import Variable
import torch.nn as nn
import torch.nn.functional as F
from torch.autograd import Variable
import matplotlib.pyplot as plt
import time
import datetime
#Model Architecture
import Model
import time

from progressbar import  ProgressBar

torch.manual_seed(1)

seasondict = {0:'Winter',
	1:"Winter",2:"Winter",3:"Spring",4:"Spring",5:"Summer",6:"Summer",
			  7:"Summer",8:"Monsoon",9:"Monsoon",10:"Autumn",11:"Autumn",12:"Winter"}


 
class DefaultParameter(object):
	
	'''
	This class for initialising default parameter.
	'''
	def __init__(self,enddate, jlasty=1, jtargety=2, trainglobal=True, 
				 trainingPath="../training_data.csv", startdate='2017-01-01', 
				 prev_time_steps=2,   averagewindow = 2,trainpercent=0.8,
				 base_path="../final/", globalmodelpath='global', 
				 localmodelpath="Brand_Center",csvpredictpath="Predicted_LSTM",
				 normalise_y=True,normalise_feat=True, COL=['y_next','B1',"B2","B3", 'Smoker Share', "roll"]
			   ):
		'''
		jlasty= 1 (int): shift combined sales value by -1,  to make a new feature 
						"last observed combined sales value" for the current date to
						predict future sales.
		
		jtargety= 2 (int): n steps ahead we want to make prediction of sales value
						   with the features of current date. 
		
		enddate = "2019-04-01" (str): Date about which you want to split data.
									eg-we will use features of "2019-04-01" ,last observed
									combined sale value of "2019-05-01"(jlasty=1) and will predict 
									combined sales value for "2019-06-01"(jtargety=2).
		
		trainglobal= True , When you want to train global model.
		
		trainingPath= Path of training csv file 
		
		sartdate =(str) date from where the training should start
		
		prev_time_steps=(int) number of time-steps from history you want to look for predicting future
		
		averagewindow =(int) number of input sequence you want to feed in one-instance of LSTM.
		trainpercent =(float between 0 to 1) percentage split between training and validation data.
		
		base_path = path of the folder where you want model and prediction file to be saved.
		
		globalmodelpath=   Name of the folder for storing global model inside base_path
							
		localmodelpath= Name of the folder for storing local model inside base_path.
						
		csvpredictpath=  Name of the folder for storing predicted csv files inside base_path.
					
		normalise_y =  True if you want to normalise y
		normalise_feat= True if you want to normalise smoker share feature
		
		'''
		
		self.HIDDEN_SIZE = 0.5
		self.COL = COL   #list(['y_past','B1',"B2","B3", 'Smoker Share', "roll"])

		self.jlasty= jlasty
		self.jtargety= jtargety
		self.enddate= enddate
		self.trainglobal= trainglobal
		print("is train global is true:{}".format(trainglobal))
		
		self.trainingPath= trainingPath
		self.startdate= startdate
		self.prev_time_steps= prev_time_steps  
		self.averagewindow= averagewindow
		self.trainpercent=  trainpercent
		self.base_path= base_path
		self.globalmodelpath= base_path+ globalmodelpath
		self.localmodelpath= base_path + localmodelpath
		self.csvpredictpath= base_path + csvpredictpath
		if not os.path.exists(self.base_path):
			os.mkdir(base_path)
		if not os.path.exists(self.globalmodelpath):
			os.mkdir(self.globalmodelpath)
		if not os.path.exists(self.localmodelpath):
			os.mkdir(self.localmodelpath)
		if not os.path.exists(self.csvpredictpath):
			os.mkdir(self.csvpredictpath)
		self.normalise_y = normalise_y
		self.normalise_feat= normalise_feat
		self.prev_time_steps=prev_time_steps
		
		print("csv files will be saved inside ", self.csvpredictpath)
		
  
class DataPrep(DefaultParameter):
	
	def prepDataFeat(self,df):
		'''
		It is used for feature engineering, creating target(y) and Return df 
		with processed features and target column.
		'''
		if self.normalise_y:
			target_max = df['Combined mean_prev'].max()
			df['max'] = [target_max]*len(df)
		else:
			target_max = 1
			df['max'] = [target_max]*len(df)

		df['Combined mean_prev'] = df['Combined mean_prev'] / target_max 
		df['Combined'] = df['Combined'] / target_max 
		df['y_curr'] = df['Combined']
		df['y_next'] =  df['y_curr'].shift(-self.jlasty) 
		df['roll'] =  (df['y_curr']+ df['Combined mean_prev'] + df['y_next'])/3

		# use for t+1 prediction value in y or -2 for t+2.
		df['y'] =  df['Combined'].shift(-self.jtargety) 
		df['Season']  =  df['ds'].apply(lambda x:seasondict[(int(x.split("-")[1])+self.jtargety)%12])

		df['Winter'] = df['Season'].apply(lambda x : 1 if x=='Winter' else 0)
		df['Summer'] = df['Season'].apply(lambda x : 1 if x=='Summer' else 0)
		df['Monsoon'] = df['Season'].apply(lambda x : 1 if x=='Monsoon' else 0)
		df['Autumn'] = df['Season'].apply(lambda x : 1 if x=='Autumn' else 0)
		df['Spring'] = df['Season'].apply(lambda x : 1 if x=='Spring' else 0)
		df.drop('Season',axis=1,inplace =True)
		#dropping the last row sice it doesn't have features
		df = df.iloc[:-1].reset_index(drop=True)
		for c in self.COL:
			if c!='Brand' and c!='Center'and c!='ds' :
				df[c] = df[c].astype(float)

		if self.normalise_feat:
			if df['Smoker Share'].max() == 0:
				 df['Smoker Share'] =  df['Smoker Share'] / 1
			else:
				df['Smoker Share'] = df['Smoker Share'] / df['Smoker Share'].max()
			
		df['ds'] = pd.to_datetime(df['ds'],yearfirst=True) 
		
		return df 

	def convertToSupervised(self,df, traintime,future_time_steps=0):
		'''
		Convert TimeSeries data into supervised data.It return supervised dataframe.
		'''
		if traintime:
			startts = -1*future_time_steps
		else:
			startts = 0
		colms_to_return =  ['Brand','Center','ds','max','y_curr']+ self.COL+ ["y"]
		for columnname in  self.COL+["y"]:
			if columnname =="ds":
				continue
			for i in range(startts,self.prev_time_steps+1):
				if i < 0:
					df["%s(month + %d)"%(columnname,(-1*i))] = df[columnname].shift(i)
					colms_to_return.append("%s(month + %d)"%(columnname,(-1*i)))
				if i >0:
					df["%s(month - %d)"%(columnname,i)] = df[columnname].shift(i)
					colms_to_return.append("%s(month - %d)"%(columnname,i))

		return df[colms_to_return]
	
	def prepDataLSTM(self,rowseries,traintime):
		'''
		It convert input(X) into a format suitable for LSTM.Return numpy-array,
		array(onebatchoneinstance ,outputs)
		'''
		#number of predictions or output , in our case we are predicting next month
		avg_win = self.averagewindow
		onebatchoneinstance = np.zeros((1, avg_win ,len(self.COL)))
		outputs = np.zeros((1,1)) 

		############Initializing This Month

		for i in range(len(self.COL)):
			onebatchoneinstance[0,-1,i] = rowseries[self.COL[i]]

		wind= avg_win -1
		for i in range(0,avg_win):
			if i !=0:
				for k in range(len(self.COL)):
					onebatchoneinstance[0,wind-i,k] = rowseries[self.COL[k]+"(month - %i)"%i]
				 
		if traintime:
			outputs[-1,0] = rowseries['y' ]
		
		return (onebatchoneinstance ,outputs)
	
	def Merge_Prediction(self,folder_path):
		files = os.listdir(folder_path)
		df = pd.DataFrame( )
		for i in range( len(files) ):
			temp = files[i].split("_")[1].split("-")
			br = temp[0]+"-"+temp[1]
			center = temp[2][:-4]
			df_ = pd.read_csv(folder_path+files[i])
			df_['Brand']  = br
			df_['Center'] = center
			df =pd.concat([df_,df]).reset_index(drop=True)

		return df

class Training(DataPrep):
	def splitTrainTest_byDate(self,df , traintime):
		'''
		It Splits TimeSeries data about end-date. Data before end-date is used as 
		Training data and after it is used as Testing data.It returns numpy array.
		'''

		allrows = [] 
		if traintime:
			col_subset = list(df.columns)
			col_subset.remove('y')
			df = df.dropna(subset=col_subset).reset_index(drop=True)
			process = False
			if len(df[df['ds']== self.enddate].index)== 1:
				idx = df[df['ds']== self.enddate].index[0]
				df = df.iloc[:idx]
				print("training local",df['Brand'].unique(),df['Center'].nunique())
				process =True
				 
			
			if len(df[df['ds']== self.enddate].index)>1:
				df['ds"'] = pd.to_datetime(df['ds']) 
				mask = (df['ds"'] > self.startdate) & (df['ds"'] < self.enddate)
				df = df.loc[mask].reset_index(drop=True)
				df = df.dropna().reset_index(drop=True)
				print("training global",df['Brand'].unique(),df['Center'].nunique())
				process = True

			if process:
				for index,row in df.iterrows():
					newrow = self.prepDataLSTM(row ,traintime)
					allrows.append(newrow)    
				
				random.shuffle(allrows)
				tot = len(allrows)
				train = int(self.trainpercent*tot)
				print("number of training example",len(allrows[:train]))
				print("number of validation example",len(allrows[train:] ))
				return allrows[:train],allrows[train:]
			else:
				print("nothing to be predicted for ",df['Brand'].unique())
				return allrows ,allrows
		
		else:
			col_subset = list(df.columns)
			col_subset.remove('y')
			df = df.dropna(subset=col_subset).reset_index(drop=True)
			process = False
			
			if len(df[df['ds']==self.enddate].index)== 1:
				idx = df[df['ds']== self.enddate].index[0]
				df = df.iloc[idx:idx+1]
				process = True
			
			if len(df[df['ds']== self.enddate].index)>1:
				df['ds"'] = pd.to_datetime(df['ds']) 
				mask = (df['ds"'] == self.enddate)
				df = df.loc[mask].reset_index(drop=True)
				process =True
			 
			if process:
				for index,row in df.iterrows():
					newrow = self.prepDataLSTM(row, traintime)
					newrow = [row['ds'],row['max'],row['y_curr'] , newrow,row['y'],row['y_next']]
					allrows.append(newrow)  
				return allrows
			else:
				return allrows
		
	def trainGlobal(self,df, traintime): 
		'''
		Training for global-model
		'''
		newdf1 = pd.DataFrame()
		brands = df['Brand'].unique()
		for b in brands:
			df_b =df[df['Brand'] == b].reset_index(drop=True)
			center = df_b['Center'].unique()
			for c in center:
				df_c = df_b[df_b['Center'] == c].reset_index(drop=True)
				df_c = self.convertToSupervised(df_c,traintime)
				col_subset = list(df_c.columns)
				col_subset.remove('y')
				df_c = df_c.dropna(subset=col_subset).reset_index(drop=True)
				# df_c = df_c.dropna()
				newdf1 = pd.concat([newdf1,df_c]).reset_index(drop=True)
		
		 
		trainset,valset = self.splitTrainTest_byDate(newdf1.copy(),traintime)
		 
		numfeatures=len(self.COL)
		epochs= 50
		lr= 0.001
		momentum = 0.6
		criterion = nn.MSELoss()
		model = Model.RNN_Attention( numfeatures,numfeatures ,1, self.HIDDEN_SIZE,2,use_cuda= False) #1 is number of output
		# model.to(device)
		
		opti = torch.optim.Adam(model.parameters(), lr=lr)
		loss_value = []
		min_loss = 200
		min_index = 0
		print("training global model for lstm with {} epoch".format(epochs))
		print("training for generalised features")
		
		pbar = ProgressBar()
		t =time.time()
		 
		for epochnum in  pbar(range(epochs)):
			print( epochnum,"th epoch")
			for (batch0 ,output) in trainset:
				opti.zero_grad()
				op = model(torch.FloatTensor(batch0) ).squeeze()

				mseloss = criterion(op  ,torch.FloatTensor(output) )
				mseloss.backward()
				opti.step()
			valloss = 0.
			
			with torch.no_grad():
				numtest = 0
				for (batch0 ,output) in valset:
					op = model(torch.FloatTensor(batch0))
					op = op.squeeze()
					mseloss = criterion(op,torch.FloatTensor(output)   )

					valloss += mseloss.item() 
					numtest += 1
			print( "Validation Loss Average: ",valloss)
			if epochnum != 0 and epochnum%10 ==0:
				torch.save(model.state_dict(), os.path.join(self.globalmodelpath 
								, "model_%d.pth"%(epochnum)))
				if min_loss > valloss:
					min_loss = valloss
					min_index = epochnum

		print("total time ",time.time()-t)
		return min_index
	
	def trainLocal(self,df ,GLOBAL_INDEX,traintime ):
		'''
		Training indivisual model for each brand center pair
		'''
		b = df['Brand'].unique()[0]
		c = df['Center'].unique()[0]

		df1  = self.prepDataFeat(df.copy())

		newdf1 = self.convertToSupervised(df1.copy(),traintime)
		trainset,valset = self.splitTrainTest_byDate(newdf1.copy(),traintime)
		 
		if len(trainset)>0:
			numfeatures=len(self.COL)
			epochs = 100
			lr = 0.001
			momentum = 0.6

			criterion = nn.MSELoss()

			model = Model.RNN_Attention( numfeatures,numfeatures ,1,self.HIDDEN_SIZE, 2)
			model.load_state_dict(torch.load(os.path.join(
									self.globalmodelpath, "model_%d.pth"%(GLOBAL_INDEX))))
			opti = torch.optim.Adam(model.parameters(), lr=lr)

			loss_value = []

			min_loss = 10
			min_index = 0
			for epochnum in range(epochs):
				for (batch0 ,output) in trainset:
					opti.zero_grad()
					op = model(torch.FloatTensor( batch0) ).squeeze()
					mseloss = criterion(op,torch.FloatTensor(output))
					mseloss.backward()
					opti.step()
				valloss = 0.
				with torch.no_grad():
					numtest = 0
					for (batch0 ,output) in valset:
						op = model(torch.FloatTensor( batch0) )
						op = op.squeeze()
						 
						mseloss = criterion(op,torch.FloatTensor(output))
						valloss += mseloss.item()
						numtest += 1
				 
				if epochnum != 0 and epochnum%10 ==0:
					torch.save(model.state_dict(), os.path.join(
						 self.localmodelpath+"/"+str(b)+"-"+str(c),
						 "model_%d.pth"%(epochnum)))
					if min_loss > valloss:
						min_loss = valloss
						min_index = epochnum

			return min_index

		else:
			return -1

class Predict_Save(Training):
	def predict_withBugetImpact(self,df,epochnum,date):
		b = df['Brand'].unique()[0]
		c = df['Center'].unique()[0]

		df1 = self.prepDataFeat(df.copy())
		newdf1 =self.convertToSupervised(df1.copy(), traintime=False )

		opfile = open( self.csvpredictpath+(str(date))+"/pred_" +  b+"-"+c +  ".csv","w")
		CSVout = csv.writer(opfile)

		modelepoch = epochnum 

		CSVout.writerow(["ds","model_epoch","max", "y_curr","last_obs(y)" ,"y", "y_pred"])

		valset = self.splitTrainTest_byDate(newdf1.copy(), traintime=False)

		filename = self.localmodelpath  + "/"+b+"-"+c+"/model_"+ str(modelepoch) + ".pth"

		weight = torch.load(filename)
		numfeatures=len(self.COL)
		model = Model.RNN_Attention( numfeatures,numfeatures ,1, self.HIDDEN_SIZE,2)
		model.load_state_dict(weight)

		for dp in valset:
			ds = dp[0]
			max_y = dp[1]
			y_curr = dp[2]
			inp1, _ = dp[3]
			inp1t = torch.FloatTensor(inp1)
			op = model(inp1t )[0]
			y = dp[4]
			y_1=dp[5]
			CSVout.writerow([ds, modelepoch, max_y, y_curr*max_y,y_1*max_y, y*max_y, op[0].item()*max_y])

		opfile.close()

	def Predict( self,df, epochnum):
		
		b= df['Brand'].unique()[0]
		c= df['Center'].unique()[0]
		
		df1 = self.prepDataFeat(df.copy())
		newdf1 = self.convertToSupervised(df1.copy(), False )
		opfile = open(self.csvpredictpath +"/pred_" +  b+"-"+c + ".csv","w")
		CSVout = csv.writer(opfile)
		modelepoch = epochnum 
		modelfunc =  self.prepDataLSTM
		
		CSVout.writerow(["ds","model_epoch","max", "y_curr",'last_obs(y)',"y","y_pred"])
		valset =  self.splitTrainTest_byDate(newdf1.copy(), False)
		
		filename =   self.localmodelpath+ "/"+b+"-"+c+"/model_"+ str(modelepoch) + ".pth"
		weights = torch.load(filename)
		numfeatures=len(self.COL)
		model = Model.RNN_Attention( numfeatures,numfeatures ,1, self.HIDDEN_SIZE,2)
		model.load_state_dict(weights)
		
		for dp in valset:
			ds=dp[0]
			max_y=dp[1]
			y_curr = dp[2]
			# print month, volume
			inp1, _ = dp[3]
			inp1t = torch.FloatTensor(inp1)
			op = model(inp1t )[0]
			y = dp[4]
			y_1 = dp[5]
			print( ds,  max_y, y_curr*max_y,y_1*max_y, y*max_y, op[0].item()*max_y)
			CSVout.writerow([ds, modelepoch, max_y, y_curr*max_y,y_1*max_y, y*max_y, op[0].item()*max_y])
		opfile.close()

	def getResultLSTM(self):
		'''
		Main function that saves and train the model and store prediction
		'''
		DF = pd.read_csv(self.trainingPath)
		for colm in DF.columns:
			try:
				DF[colm] = DF[colm].astype(float)
			except:
				pass

		if self.trainglobal:
			df = DF.copy()
			brands = df['Brand'].unique()
			brands = brands[brands != 'Brand-37258']
			df_new=pd.DataFrame()
			for b in (brands) :
				df_b = df[df['Brand'] == b].reset_index(drop=True)
				center=df_b['Center'].unique()
				for c1 in center  : 
					df_c = df_b[df_b['Center'] == c1].reset_index(drop=True)
					df_temp = self.prepDataFeat(df_c.copy())
					df_new=pd.concat([df_new,df_temp] ).reset_index(drop=True)
			df = df_new.copy()    
			min_index = self.trainGlobal(df.copy(),True)

			f =open(self.base_path+"min_index" +".txt","w")
			f.write( str(min_index) )
			f.close()
		else:
			f = open(self.base_path+ "min_index.txt","r")
			min_index = int(f.read())
	
		global_index =   min_index
		if global_index == 0:
			global_index = 10

		DF = pd.read_csv(self.trainingPath)
		df = DF.copy()
		brands = df['Brand'].unique()
		brands = brands[brands != 'Brand-37258']
		for colm in df.columns:
			try:
				df[colm] = df[colm].astype(float)
			except:
				pass
		pbar_ = ProgressBar()
		for b in   pbar_(brands ) :
			df_b = df[df['Brand'] == b].reset_index(drop=True)
			center=df_b['Center'].unique()
			for c1 in  center:
				if not os.path.exists( self.localmodelpath+ "/"+b+"-"+c1+"/"):
					os.mkdir(self.localmodelpath+ "/"+b+"-"+c1+"/")
				df_c = df_b[df_b['Center']==c1].reset_index(drop=True)
				index = self.trainLocal(df_c.copy(),global_index,True )
				 
				if index==-1:
					continue
				if index == 0:
					index = 10
				self.Predict(df_c.copy(), index)
		
		csv_df = self.Merge_Prediction(self.csvpredictpath+"/")
		csv_df.to_csv(self.base_path+"Prediction.csv",index=False)
		print("LSTM-Prediction : saved to {}".format(self.base_path+"Prediction.csv"))
		return csv_df

				
	def budgetImpact(self,date):
		'''
		Function returns dataframe having demand value of sales calculated after incoorporating
		budget factor for the same month in previous year.

		date = "2018-04-01" if you want to get sales value for 2019-06-01 (when jtargety=2) (2018 is 
				used since we are first intrested in getting predicted value for 2018-06-01).
		'''
		self.enddate = date
		 
		if not os.path.exists(self.csvpredictpath+(str(date))+"/"):
			os.mkdir(self.csvpredictpath+(str(date))+"/")

		df = pd.read_csv(self.trainingPath)
		brands = df['Brand'].unique()
		brands = brands[brands != 'Brand-37258']
		for b in   (brands ) : 
			df_b = df[df['Brand'] == b].reset_index(drop=True)
			center=df_b['Center'].unique()
			for c1 in   center : 
				try:
					df_c = df_b[df_b['Center']==c1].reset_index(drop=True)
					df2 = pd.read_csv( self.csvpredictpath + "/pred_"+b+"-"+c1+".csv")
					index =int( df2['model_epoch'].values[0])
					self.predict_withBugetImpact(df_c.copy(), index,self.enddate)
				except Exception as E:
					print(b,c1,E)

from config import config
if __name__ == '__main__':
	#  Training data prep for classification

	df = pd.read_csv(config['TrainingClassifyPath'])
	print(config['model_lstm_param'] )

	Result =   Predict_Save(**config['model_lstm_param'] )  
	lstm_result= Result.getResultLSTM() 