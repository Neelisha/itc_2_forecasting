import pandas as pd
import numpy as np
import os 
from config import config
from progressbar import ProgressBar

pbar = ProgressBar() 
def replace_outliers(df_temp ):
    high_quantiles = df_temp.quantile(0.99)
    outliers_high = (df_temp > high_quantiles)
    df_temp[outliers_high] = np.nan
    df_temp.fillna(high_quantiles, inplace=True)
    return df_temp
            

def bucket_features(df_temp, cols, brand):
    
    df1_temp = pd.DataFrame(columns=['Brand Name', 'Date Stamp', 
                                    'B1', 'B2', 'B3','count_shop',
                                     'ShopLevelTotSale'
                                     ]
                            )
    for i in  range(len(cols)-3):
        prev_claim=list(df_temp[cols[3+i-1]])
        curr_claim=list(df_temp[cols[3+i]])
        y=[]
        for z in range(len(prev_claim)):
            if prev_claim[z]==0 and curr_claim[z]==0:
                continue
            if curr_claim[z]==0 and prev_claim[z]!=0:
                y.append(-1.)
            else:
                y.append((curr_claim[z]-prev_claim[z])/float(curr_claim[z]))
        if len(y)>0:
            B1= np.array(y)<-0.1
            B1=np.array(B1.astype(int))
            B3=np.array(np.array(y)>0.1)
            B3=np.array(B3.astype(int))
            B2= (B3+B1)
            B2=B2==0
            B2=np.array(B2.astype(int))
             
            df1_temp = df1_temp.append(pd.Series([brand,cols[3+i],B1.sum()/len(B1),B2.sum()/len(B2),
                                                B3.sum()/len(B3),len(y),sum(curr_claim)],
                                        index=['Brand Name', 'Date Stamp', 'B1',
                                               'B2', 'B3','count_shop','ShopLevelTotSale']),
                                        ignore_index=True)
    df1_temp = df1_temp.append(pd.Series([brand,config['GlobalDates'][-1],'aaa','aaa',
                                            'aaa','aaa','aaa'],
                                        index=['Brand Name', 'Date Stamp', 'B1',
                                               'B2', 'B3','count_shop','ShopLevelTotSale']),
                                        ignore_index=True)

    return df1_temp
                    
 
def process_data( cities, rawFilePath,ProcessedPath ):
    
    if not os.path.exists(ProcessedPath):
        os.mkdir(ProcessedPath)
    pbar = ProgressBar() 
    all_file = os.listdir(rawFilePath)
    for  k in pbar(all_file) :
        city = k.split(" ")[0] 
 
        if not(os.path.exists( ProcessedPath + city )):
            os.mkdir(ProcessedPath+city ) 
        
        # for k in  (all_file): 
            # if city in k:
        df = pd.read_csv( rawFilePath + k )
        df['Date Stamp'] = df['MMYY'].replace(config['DateStamp'])
        brands = df['Brand Name'].unique()
        df2 = df.pivot( columns='Date Stamp',values="Retail Claimed Sales"
                      ).fillna(0)
        DF = df.merge(df2,left_index=True,right_index=True)
        DF.drop(['Date Stamp','Retail Claimed Sales'],axis=1,inplace=True
               )
        col = DF.columns
        final_df = DF.groupby(['Brand Name','Shop ID'])[col[2:]].sum().reset_index()
        final_df = final_df[['Brand Name','Shop ID']+config['GlobalDates'][:-1]]

        cols = final_df.columns
        df1 = pd.DataFrame(columns=['Brand Name', 'Date Stamp', 'B1', 'B2', 'B3','count_shop',
                             'ShopLevelTotSale']
                          )
        for j in brands:
            dd = final_df[final_df['Brand Name'] == j].reset_index(drop=True)
            dd = replace_outliers(dd.copy())
            df1_temp = bucket_features(dd.copy(),cols,j)
            df1 = pd.concat([df1,df1_temp] )
            df1.to_csv( ProcessedPath + city + "/claim.csv",index=False)



def city_wise_combine(df_temp , ProcessedPath ):
    df_temp.rename(columns=config['MonthToDate'],inplace=True)
    cities=list(df_temp.Center.unique())
    pbar = ProgressBar()
    for city in pbar(cities):
        print(city)
        df1=df_temp[df_temp['Center']==city].reset_index(drop=True)
        if not os.path.exists(ProcessedPath +city.capitalize()+"/"):
            os.mkdir( ProcessedPath+city.capitalize()+"/")
        df1.to_csv( ProcessedPath+city.capitalize()+"/combined.csv",index=False)
    print("Combined csv saved citywise")
 

def make_supervised(df,p):
    DF=pd.DataFrame()
    DF['B1']=df['B1']
    DF['B2']=df['B2']
    DF['B3']=df['B3']
    DF['count_shop'] = df['count_shop']
    DF['ShopLevelTotSale'] = df['ShopLevelTotSale']
    DF['combined']=df['combined']
    DF['prev_combined']=df['combined'].shift(1)
    DF["Date Stamp"]=df["Date Stamp"]
    DF.at[ 0,'prev_combined'] = p   
    DF.dropna(inplace=True)
    return DF



def combine_claim(cities ,ProcessedPath):
    train_df = pd.DataFrame()
    pbar = ProgressBar()
    for city in  pbar(cities):
        combined_df=pd.read_csv(ProcessedPath + city+"/combined.csv")
        combined_df.rename(columns={'Brand':'Brand Name'},inplace=True)
        claimed_df=pd.read_csv( ProcessedPath + city+"/claim.csv")
         
         #considering brand having values for all dates in combined data
        df_comb=combined_df[~combined_df.isnull().any(axis=1)].reset_index(drop=True)
        brand_com=set(df_comb['Brand Name'].unique())
        claimed_brand=set(claimed_df['Brand Name'].unique())
        brands=list(claimed_brand.intersection(brand_com))
        columns=df_comb.columns
        result_df2=pd.DataFrame()
        #     df_comb.shape[0]

        for i in range( df_comb.shape[0] ) :
            br =df_comb.iloc[i]['Brand Name']
            if br in brands:
                DF=pd.DataFrame()
                df_clm=claimed_df[claimed_df['Brand Name']== br].reset_index(drop=True)
                df_com2=combined_df[ combined_df['Brand Name']== br].reset_index(drop=True)
                
                df_com2=df_com2.T.reset_index(drop=True)
 
                df_com2_copy=df_com2.copy()

                df_com2=df_com2.iloc[2:].reset_index(drop=True)# data is from 2 feb 2017 in many claim data
               
                time_df=pd.DataFrame()

                time_df['Date Stamp']= config['GlobalDates']

                df=pd.merge(time_df,df_clm,on="Date Stamp",left_index=False,right_index=False,how='outer')

                df['combined']=df_com2[0] 
                 

                prev_combined=df_com2_copy[0][0]

                df =make_supervised(df,prev_combined) 
                indexes=df['Date Stamp']
                df['prev_combined']=df['prev_combined'].astype("float")
                df['combined']=df['combined'].astype("float")
                df['center']=[city]*len(df)
                df['brand']=[br]*len(df)
                df['ds']=indexes

                train_df=pd.concat([df,train_df])

    train_df.drop("Date Stamp",axis=1,inplace=True)
    df = train_df.copy()
    cols = ['center','brand','ds','prev_combined','B1','B2','B3','count_shop','ShopLevelTotSale','combined']
    df = df[cols]
    df.rename(columns={'center':'Center','brand':'Brand','prev_combined':'Combined mean_prev','combined':'Combined'}, 
              inplace=True)
    return df.reset_index(drop=True)

 
def own_brand_taste_combine(feature_df,date):
    ownbrand_taste_df = pd.DataFrame(columns=['Center', 'ds','Measure','Trend'])
    for i in range(feature_df.shape[0]):
        temp = pd.DataFrame()
        temp['Trend'] = list(feature_df.iloc[i][9:])
        temp['Center'] = feature_df.iloc[i][0]
        temp['Measure'] = feature_df.iloc[i][1]
        temp['ds'] = date
        cols = temp.columns.tolist()
        temp = temp[cols]
        ownbrand_taste_df = pd.concat([ownbrand_taste_df, temp]).reset_index(drop=True)
    return ownbrand_taste_df

def features_trend_combine(feature_df,date):
    feature_trend_df = pd.DataFrame(columns=['Center','Brand','ds','Measure','Trend'])
    for i in range(feature_df.shape[0]):
        # print(date)
        temp = pd.DataFrame()
        temp['Trend'] = list(feature_df.iloc[i][15:])
        temp['Center'] = feature_df.iloc[i][1]
        temp['Brand'] = feature_df.iloc[i][0]
        temp['Measure'] = feature_df.iloc[i][2]
        temp['ds'] = date
        cols = temp.columns.tolist()
        temp = temp[cols]
        feature_trend_df = pd.concat([feature_trend_df, temp]).reset_index(drop=True)
    return feature_trend_df

 
def feature_combine(df, FeatureFilePath):
    market = df['Center'].unique().tolist()
    brand = df['Brand'].unique().tolist()
    feat = pd.read_excel( FeatureFilePath)

    #naming error in imrb data is corrected
    feat['Market'].replace('Kolhapur','Kohlapur',inplace=True)
 
    feat = feat[feat['Market'].isin(market)].reset_index(drop=True)
    feat = feat[feat['Masked Brand'].isin(brand)].reset_index(drop=True)

    if 'Unnamed: 0' in feat.columns:
    	feat.drop('Unnamed: 0',axis=1,inplace=True)
    
    print(feat.columns)
    date =  config['GlobalDates']
    feat[config['LastMonth']] = ['aaa']*feat.shape[0]
    features_trend_df = features_trend_combine(feat.copy(),date)
 
    feature_df = pd.pivot_table( features_trend_df, index=['Brand','Center','ds'],
                                  columns='Measure', values='Trend', aggfunc='first').reset_index()   
    feature_df.rename(columns={'Own - Good Taste':'factor_Own - Good Taste',
                   'Own - Just About Ok':'factor_Own - Just About Ok',
                  'Own - Poor Taste':'factor_Own - Poor Taste' },
          inplace=True)
 
    feature_df = feature_df.reset_index(drop=True)
    cols_to_process = [colm for colm in feature_df.columns if colm not in ['Brand','Center','ds']]
    print(cols_to_process)
    feature_df.replace('LS',0,inplace=True)
    for c in cols_to_process:
    	feature_df[c]=feature_df[c].apply(lambda x:str(x).strip())
    	# feature_df[c] = feature_df[c].astype(float)
    feature_df.replace('-',0,inplace=True)
    feature_df.fillna(0,inplace=True)
    return feature_df

 
def Update_retail_claim_file( MMYYlist,oldRetailFolder,Newretailexel,Destination_folder):
    df = pd.read_excel(Newretailexel )
    print(df.head())
    df = df[df['MMYY'].isin( MMYYlist)]
    df.reset_index(drop=True,inplace=True)
    destination_folder=  Destination_folder
    if not os.path.exists(destination_folder):
        os.mkdir(destination_folder)
    df['Centre'].replace('Kolhapur','Kohlapur',inplace=True)
    cities = df['Centre'].unique()
    df.to_excel(Newretailexel,index=False)
    oldRetailFiles = os.listdir(oldRetailFolder)
    for city in pbar(cities) :
        for file in  oldRetailFiles:
            if city in file:
                try:
	                df_temp = df[df['Centre']==city].reset_index(drop=True)
	                df_retail =  pd.read_csv((oldRetailFolder+file))
	                df_retail = pd.concat([df_retail,df_temp]).reset_index(drop=True)
	                 
	                df_retail.to_csv(( destination_folder
	                                  +city+' Retailer Claimed Sales Data.csv'),
	                                 index=False)
                except:
	                pass


def   Prep_forecast_training_data():
     
    print("Key pair value for Renaming brand-name of ITC and Retail File :{}".format(config['RenameBrandCombined']))
    
    df = pd.read_excel(config['NewItcExcel'])
    df['City'] = df['City'].apply(lambda x: x.capitalize())
    df['City'].replace('Kolhapur','Kohlapur',inplace=True)
    df['City'].replace('Bhubaneswar','Bhubaneshwar',inplace=True)
    df['City'].replace('Vijayawada','Vijaywada',inplace=True)
    df['Brand-Codes'].replace(config['RenameBrandCombined'],inplace=True)
    print("check whether Brand names are renamed or not in ITC File: It should be empty")
    print(df[df['Brand-Codes'].isin(list(config['RenameBrandCombined'].keys()))].shape[0])
    df.to_excel(config['NewItcExcel'],index=False)

    # rename brand name in retail data
     
    df = pd.read_excel(config['NewRetailExcel'])
    df['Centre'] = df['Centre'].apply(lambda x: x.capitalize())
    df['Centre'].replace('Kolhapur','Kohlapur',inplace=True)
    df['Centre'].replace('Bhubaneswar','Bhubaneshwar',inplace=True)
    df['Centre'].replace('Vijayawada','Vijaywada',inplace=True)
    df['Brand Name'].replace(config['RenameBrandCombined'],inplace=True)
    df['Brand Name'].replace(config['RenameBrandAlert'],inplace=True)

    print("check whether Brand names are renamed or not in Retail File: It should be empty")
    print(df[df['Brand Name'].isin(list(config['RenameBrandCombined'].keys()))].shape[0])
    print(df[df['Brand Name'].isin(list(config['RenameBrandAlert'].keys()))].shape[0])
    df.to_excel(config['NewRetailExcel'],index=False)


    # rename brand name in feature data
    print("Key pair value for Renaming brand-name of Alert File  :{}".format(config['RenameBrandAlert']))
    df = pd.read_excel(config['FeatureFilePath'])
    df['Market'] = df['Market'].apply(lambda x: x.capitalize())
    df['Market'].replace('Kolhapur','Kohlapur',inplace=True)
    df['Market'].replace('Bhubaneswar','Bhubaneshwar',inplace=True)
    df['Market'].replace('Vijayawada','Vijaywada',inplace=True)
    df['Masked Brand'].replace(config['RenameBrandAlert'],inplace=True)
    print("check whether Brand names are renamed or not in Alert File: It should be empty")
    print(df[df['Masked Brand'].isin(list(config['RenameBrandAlert'].keys()))].shape[0])
    df.to_excel(config['FeatureFilePath'],index=False)

     
    if not os.path.exists(config['ProcessedPath']):
    	os.mkdir(config['ProcessedPath'])

    print("adding new months data in old retail file")
    Update_retail_claim_file(config['MMYYlist'], config['OldRetailFolder'] ,config['NewRetailExcel'], config['UpdatedRetailPath'] )
     
    df = pd.read_excel(config['NewItcExcel'])

    #before dropping check between first/last row to keep duplicate if contain data
    df.drop_duplicates(keep='last',inplace=True)
    df.to_excel(config['NewItcExcel'],index=False)
    col = df.columns
    col = col[2:]

    #dropping same rows that have same brand-codes in same city : check which row to keep among duplicates
    df.drop_duplicates(subset = ['City','Brand-Codes'],keep='last',inplace=True)
    df['City'].replace('Kolhapur','Kohlapur',inplace=True)  
    cities = list(df.City.unique())

    print("cities to process: {} and it's count: {} ".format(cities,len(cities)))
    print("Processing retail file to make Bucket features and store it as claim.csv at city level")
    process_data(cities, config['UpdatedRetailPath'],config['ProcessedPath'] )
    

    print("spliting ITC combined excel file citiwise and storing it as combined.csv")
    city_wise_combine(df.copy(),config['ProcessedPath'])
    
    print("combining bucket_features of claim.csv and sale value of combined.csv")
    df = combine_claim( cities,config['ProcessedPath'])
    
    print("addition of external features with above combined df")
    df2 = feature_combine(df.copy(),config['FeatureFilePath'])
    print(df.head())
    print(df2.head())
    print("now  preprocessing final df")
    final_df = df2.merge(df ,on=['Brand','Center','ds'])

    # #droping brand & center having less than 10 data points

    brand_center = final_df.groupby(['Brand','Center']).size().reset_index()

    final_df = final_df.merge(brand_center,on=['Brand','Center'])

    final_df = final_df[final_df[0]>10].reset_index(drop=True)

    final_df.drop(0,axis=1,inplace=True)
    final_df['Brand--Center'] = final_df['Brand']+'--'+final_df['Center']
    print("dropping rows having consecutive 0 in starting combined saled for every brand center pair")
 
    brcen =  final_df[(final_df['ds']=="2017-02-01") &(final_df['Combined']==0)].groupby(['Brand','Center']).size().reset_index()

    correct_data = pd.DataFrame()
    havZero_df = pd.DataFrame()

    brand_center=[]
    
    for i in range(len(brcen)):
        br = brcen.iloc[i]['Brand']
        cen = brcen.iloc[i]['Center'] 
        brand_center.append(br+"--"+cen)
        _df = (final_df['Brand']==br ) & (final_df['Center']==cen)
        df2 = final_df[_df].reset_index(drop=True)
        combined = df2['Combined'].values
        for j in range(len(combined)):
            if not combined[j]==0:
                break
        
        df2 = df2.iloc[j:]
        havZero_df = pd.concat([havZero_df,df2])
        havZero_df =havZero_df.reset_index(drop=True)

    
    correct_data = final_df[~final_df['Brand--Center'].isin(brand_center)]
    final_df2 = pd.concat([havZero_df,correct_data]).reset_index(drop=True)
    final_df2.drop('Brand--Center',axis=1,inplace=True)

    #dropping less number of examples
    brand_center = final_df2.groupby(['Brand','Center']).size().reset_index()
    final_df2 = final_df2.merge(brand_center,on=['Brand','Center'])
    final_df2= final_df2[final_df2[0]>10].reset_index(drop=True)

    final_df2.drop(0,axis=1,inplace=True)
    final_df2.to_csv( config['TrainingCsvPath'],index=False)

    print("processed file is saved at {}".format(config['TrainingCsvPath']))
     
